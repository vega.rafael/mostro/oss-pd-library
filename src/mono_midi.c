/////////////////////////////////////////////////////////////////////////
// 
//  Monophonic Midi Notes
//  Copyright (C) 2015 Rafael Vega <contacto@rafaelvega.co>
//  Copyright (C) 2015 Daniel Gómez <danielgomezmarin@gmail.com>
//
//  This program is free software: you can redistribute it and/or modify 
//  it under the terms of the GNU General Public License as published by 
//  the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
//  
//  This program is distributed in the hope that it will be useful, but 
//  WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  General Public License for more details.  
//  
//  You should have received a copy of the GNU General Public License 
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
/////////////////////////////////////////////////////////////////////////

#include "m_pd.h"
#include <uthash.h>

/////////////////////////////////////////////////////////////////////////
// Definitions
//

// Max simultaneous notes
#define NOTES_MAX 128

// A data structure that represents a note.
typedef struct note{
   int midi_number;
   int velocity;
   UT_hash_handle hh;
} note;

// A data structure to hold the PD object internal state 
typedef struct mono_midi {
   t_object x_obj;
   t_outlet *outlet0;
   t_outlet *outlet1;
   
   // Keep track of velocity messages that arrive on right inlet
   float velocity;
   
   // Pre-allocated note objects
   note* notes_pool;
   note** notes_pool_available;
   int notes_pool_available_index;
   
   // Hash table to keep track of active (pressed) notes
   note* active_notes;
} t_mono_midi;


/////////////////////////////////////////////////////////////////////////
// Forward declarations
//
static void notes_pool_deallocate(t_mono_midi* x);
static inline note* notes_pool_get_note(t_mono_midi* x);
static inline void notes_pool_return_note(t_mono_midi* x, note* n);
static void active_notes_init(t_mono_midi* x);
static void active_notes_deinit(t_mono_midi* x);
static inline void active_notes_add(t_mono_midi* x, int midi_number, int velocity);
static inline void active_notes_remove(t_mono_midi* x, int midi_number);
static inline note* active_notes_get_last(t_mono_midi* x);
static inline int active_notes_count(t_mono_midi* x);


/////////////////////////////////////////////////////////////////////////
// Preallocated pool of note objects.
//
static int notes_pool_allocate(t_mono_midi* x){
   x->notes_pool = malloc(NOTES_MAX*sizeof(note));
   if(!x->notes_pool) return 1;

   x->notes_pool_available = malloc(NOTES_MAX*sizeof(note*));
   if(!x->notes_pool_available) return 1;

   // Initialize note objects and put them in the "available" array.
   // (All notes are available initially)
   int i;
   for(i=0; i<NOTES_MAX; ++i){
      note* n = (x->notes_pool+i);
      n->midi_number = 0;
      n->velocity = 0;
      *(x->notes_pool_available+i) = (x->notes_pool+i);
   }
   x->notes_pool_available_index = NOTES_MAX-1;

   return 0;
}

static void notes_pool_deallocate(t_mono_midi* x){
   free(x->notes_pool);   
   free(x->notes_pool_available);   
   x->notes_pool = NULL;
   x->notes_pool_available = NULL;
   x->notes_pool_available_index = 0;
}

static inline note* notes_pool_get_note(t_mono_midi* x){
   if(x->notes_pool_available_index<=0) return NULL;

   note* n = *(x->notes_pool_available+x->notes_pool_available_index);
   x->notes_pool_available_index--;
   return n;
}

static inline void notes_pool_return_note(t_mono_midi* x, note* n){
   if(x->notes_pool_available_index>=NOTES_MAX) return;

   x->notes_pool_available_index++;
   *(x->notes_pool_available+x->notes_pool_available_index) = n;
}

/////////////////////////////////////////////////////////////////////////
// Hash map to keep track of pressed notes and their order.
// 


static void active_notes_init(t_mono_midi* x){
   // Add notes and remove them so that any dynamic allocation happens 
   // now
   int i;
   for(i=0; i<NOTES_MAX; ++i){
      active_notes_add(x, i,i);
   }

   for(i=0; i<NOTES_MAX; ++i){
      active_notes_remove(x, i);
   }
}

static void active_notes_deinit(t_mono_midi* x){
   HASH_CLEAR(hh, x->active_notes);
}

static inline void active_notes_add(t_mono_midi* x, int midi_number, int velocity){
   note* new_note = notes_pool_get_note(x);
   if(!new_note) return;

   new_note->midi_number = midi_number;
   new_note->velocity = velocity;
   
   HASH_ADD_INT(x->active_notes, midi_number, new_note);

   /* note* replaced = NULL; */
   /* HASH_REPLACE_INT(x->active_notes, midi_number, new_note, replaced); */
   /* if(replaced){ */
   /*    notes_pool_return_note(replaced); */
   /* } */
}

static inline void active_notes_remove(t_mono_midi* x, int midi_number){
   note* delete_me;
   HASH_FIND_INT(x->active_notes, &midi_number, delete_me);
   if(delete_me){
      HASH_DEL(x->active_notes, delete_me);
      notes_pool_return_note(x, delete_me);
   }
}

static inline int active_notes_count(t_mono_midi* x){
   return HASH_COUNT(x->active_notes);
}

static inline note* active_notes_get_last(t_mono_midi* x){
   note* last = NULL;
   note* n;
   for(n=x->active_notes; n!=NULL; n=(struct note*)(n->hh.next)){
      last = n;
   }
   return last;
}

/* static void active_notes_print(t_mono_midi* x){ */
/*    note* n; */
/*    for(n=x->active_notes; n!=NULL; n=(struct note*)(n->hh.next)){ */
/*       printf("Note number %u: velocity %i\n", n->midi_number, n->velocity); */
/*    } */
/* } */


/////////////////////////////////////////////////////////////////////////
// Receive PD Messages
// 

// Note: Right inlet is handled automatically. Floats that arrive there
// will be assigned to x->velocity. Also, if a pair of floats arrive
// to the left inlet, they will be automatically distributed to both 
// inlets

// Received MIDI note on left inlet.
static void mono_midi_float(t_mono_midi* x, t_float f) {
   if(f>127) f=127; 
   if(f<0) f=0; 

   if(x->velocity<0) x->velocity=0; 
   if(x->velocity>127) x->velocity=127;

   if(x->velocity > 0){  // Note on
      active_notes_add(x, f, x->velocity);
      outlet_float(x->outlet1, x->velocity);
      outlet_float(x->outlet0, f);
   }
   else{ // Note off 
      if(active_notes_count(x) == 1){
         active_notes_remove(x, f);
         outlet_float(x->outlet1, 0);
         outlet_float(x->outlet0, f);
      }
      else{
         note* previous_note = active_notes_get_last(x);
         active_notes_remove(x, f);

         if(previous_note && f==previous_note->midi_number){
            note* n = active_notes_get_last(x);
            outlet_float(x->outlet1, n->velocity);
            outlet_float(x->outlet0, n->midi_number);
         }
      }
   }
}

/////////////////////////////////////////////////////////////////////////
// PD constructor, destructor
//

// A pointer to the class object.
t_class *mono_midi_class;

static void *mono_midi_new(void) {
   t_mono_midi *x = (t_mono_midi *)pd_new(mono_midi_class);

   x->outlet0 = outlet_new(&x->x_obj, &s_float);
   x->outlet1 = outlet_new(&x->x_obj, &s_float);
   floatinlet_new(&x->x_obj, &x->velocity); // Second inlet sets the velocity automatically when it receives a float atom.

   x->notes_pool = NULL;
   x->notes_pool_available = NULL;
   x->notes_pool_available_index = 0;
   x->active_notes = NULL;

   x->velocity = 0;

   notes_pool_allocate(x);
   active_notes_init(x);

   return (void *)x;
}

static void mono_midi_free(t_mono_midi *x) { 
   active_notes_deinit(x);
   notes_pool_deallocate(x);
}

/////////////////////////////////////////////////////////////////////////
// PD class definition
// 
void mono_midi_setup(void) {
   mono_midi_class = class_new(gensym("mono_midi"), (t_newmethod)mono_midi_new, (t_method)mono_midi_free, sizeof(t_mono_midi), CLASS_DEFAULT, (t_atomtype)0);
   class_addfloat(mono_midi_class, mono_midi_float);
}
