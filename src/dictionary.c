/////////////////////////////////////////////////////////////////////////
//
//  JSON Dictionary
//  Copyright (C) 2015 Rafael Vega <contacto@rafaelvega.co>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
//
//  This program is distributed in the hope that it will be useful, but
//  WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
/////////////////////////////////////////////////////////////////////////

#include "m_pd.h"

#include <jansson.h>
#include <string.h>

/////////////////////////////////////////////////////////////////////////
// Data
//
typedef struct dictionary {
   t_object x_obj;
   t_canvas* canvas;

   t_outlet* outlet0;
   t_outlet* outlet1;

   json_t* json_object;
   json_t* json_array;
   int size;
} t_dictionary;

// A pointer to the class object.
t_class* dictionary_class;

/////////////////////////////////////////////////////////////////////////
// Util
//
static void dictionary_init_array(t_dictionary* x) {
   x->json_array = json_array();
   int size = x->size;
   x->size = 0;
   int i;
   for (i = 0; i < size; ++i) {
      json_t* o = json_object();
      json_object_set_new(o, "__i__", json_integer(i));
      json_array_append_new(x->json_array, o);
   }
   x->size = size + 1;
}

/////////////////////////////////////////////////////////////////////////
// Receive PD Messages
//

// Received "bang" message.
/* static void dictionary_bang(t_dictionary* x) { */
/*    (void)x; */
/*  */
/*    const char *key; */
/*    json_t *value; */
/*    json_object_foreach(x->json_object, key, value) { */
/*       // Output keys and values as messages.  */
/*       // Create a string like "this is the key value", convert it to a */
/*       // binbuf and use binbuf methods to convert it to an array of atoms */
/*       // which can be output. */
/*       char message_string[256]; */
/*       strcpy(message_string, key); */
/*       strcat(message_string, " "); */
/*       strcat(message_string, json_string_value(value)); */
/*       t_binbuf *message = binbuf_new();  */
/*       binbuf_text(message, message_string, strlen(message_string)); */
/*       int size = binbuf_getnatom(message); */
/*       t_atom *list = binbuf_getvec(message); */
/*       outlet_list(x->outlet0, gensym("list"), size, list); */
/*    } */
/* } */

// Received "set" message, with parameter list
static void dictionary_set(t_dictionary* x, t_symbol* s, int argc,
                           t_atom* argv) {
   char key[256];
   char value[256];
   char str[256];
   strcpy(key, "");
   strcpy(str, "");

   int i;
   for (i = 0; i < argc - 1; ++i) {
      atom_string(argv + i, str, 256);
      if (strlen(key) + strlen(str) < 255) {
         strcat(key, str);
         if (i < argc - 2) { strcat(key, " "); }
      }
      else {
         error("json/dictionary: maximum length for a key is 256 characters.");
      }
   }

   atom_string(argv + argc - 1, value, 256);

   json_object_set_new(x->json_object, key, json_string(value));

   (void)s;
}

// Received "writefile" message, with symbol argument
static void dictionary_writefile(t_dictionary* x, t_symbol* s) {
   char path[MAXPDSTRING];
   canvas_makefilename(x->canvas, s->s_name, path, MAXPDSTRING);
   FILE* f = fopen(path, "w");
   if (f == NULL) {
      error("json/dictionary: could not write to file");
      return;
   }
   if (json_dumpf(x->json_array, f, JSON_INDENT(3)) == -1) {
      error("json/dictionary: could not write to file");
   }
   fclose(f);
   outlet_bang(x->outlet1);
}

// Received "readfile" message, with symbol argument
static void dictionary_readfile(t_dictionary* x, t_symbol* s) {
   int size = x->size;
   x->size = 0;

   char path[MAXPDSTRING];
   canvas_makefilename(x->canvas, s->s_name, path, MAXPDSTRING);
   FILE* f = fopen(path, "r");
   if (f == NULL) {
      error("json/dictionary: could not open file");
      return;
   }

   json_decref(x->json_array);
   x->json_array = NULL;
   json_error_t e;
   x->json_array = json_loadf(f, 0, &e);
   if (x->json_array == NULL) {
      error("json/dictionary: could not read file");
      x->size = size;
      dictionary_init_array(x);
   }
   fclose(f);

   x->size = json_array_size(x->json_array);
   outlet_bang(x->outlet1);
}

// Received "write" message, with float argument
static void dictionary_write(t_dictionary* x, float f) {
   if (f < 0 || f >= x->size) {
      error("json/dictionary: write index is out of bounds");
      return;
   }
   json_t* new_object = json_deep_copy(x->json_object);
   json_t* i = json_integer(f);
   json_object_set_new(new_object, "__i__", i);
   json_array_set_new(x->json_array, f, new_object);
}

// Received "read" message, with float argument
static void dictionary_read(t_dictionary* x, float f) {
   if (f < 0 || f >= x->size) {
      error("json/dictionary: read index is out of bounds");
      return;
   }

   // Output messages
   json_t* obj = json_array_get(x->json_array, f);
   const char* key;
   json_t* value;
   json_object_foreach(obj, key, value) {
      if (strcmp(key, "__i__") != 0) {
         char message_string[256];
         strcpy(message_string, key);
         strcat(message_string, " ");
         strcat(message_string, json_string_value(value));
         t_binbuf* message = binbuf_new();
         binbuf_text(message, message_string, strlen(message_string));
         int size = binbuf_getnatom(message);
         t_atom* list = binbuf_getvec(message);
         outlet_list(x->outlet0, gensym("list"), size, list);
      }
   }

   // Load the outputed object into x->json_object
   json_decref(x->json_object);
   x->json_object = NULL;
   x->json_object = json_deep_copy(obj);
}

/////////////////////////////////////////////////////////////////////////
// Constructor, destructor
//

static void* dictionary_new(float f) {
   if (f < 0) {
      error(
          "json/dictionary: Creation argument (size) must be a positive "
          "integer.");
      return NULL;
   }

   t_dictionary* x = (t_dictionary*)pd_new(dictionary_class);

   x->canvas = canvas_getcurrent();
   x->outlet0 = outlet_new(&x->x_obj, &s_anything);
   x->outlet1 = outlet_new(&x->x_obj, &s_anything);

   x->size = f;
   x->json_object = json_object();
   dictionary_init_array(x);

   return (void*)x;
}

static void dictionary_free(t_dictionary* x) {
   json_decref(x->json_object);
   json_decref(x->json_array);
}

/////////////////////////////////////////////////////////////////////////
// Class definition
//
void dictionary_setup(void) {
   dictionary_class =
       class_new(gensym("dictionary"), (t_newmethod)dictionary_new,
                 (t_method)dictionary_free, sizeof(t_dictionary), CLASS_DEFAULT,
                 A_DEFFLOAT, (t_atomtype)0);
   class_addmethod(dictionary_class, (t_method)dictionary_set, gensym("set"),
                   A_GIMME, 0);
   class_addmethod(dictionary_class, (t_method)dictionary_write,
                   gensym("write"), A_DEFFLOAT, 0);
   class_addmethod(dictionary_class, (t_method)dictionary_read, gensym("read"),
                   A_DEFFLOAT, 0);
   class_addmethod(dictionary_class, (t_method)dictionary_writefile,
                   gensym("writefile"), A_DEFSYMBOL, 0);
   class_addmethod(dictionary_class, (t_method)dictionary_readfile,
                   gensym("readfile"), A_DEFSYMBOL, 0);
   /* class_addbang(dictionary_class, (t_method)dictionary_bang); */
}
