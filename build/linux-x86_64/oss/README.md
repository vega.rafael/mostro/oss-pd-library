# Outer Space Sounds Pd Library

A set of Pd externals and abstractions used in Outer Space Sounds' projects

## Compiling

The makefile in the src directory should take care of everything:

    sudo apt-get install build-essential automake puredata-dev libtool  # dependency
    cd src
    make distclean
    make

If everything went fine, the directory build/<architecture>/oss should be created, with all the library objects and help files in it.

## Usage

Instead of installing the library to a system directory, it is recommended to add the library to a sub folder in your project (optionally as a git submodule), add a [declare -path path/to/subfolder] object in your patch and use the library objects with the 'oss/' prefix. For example:

![example patch](docs/example.png)

Note that there are different directories for different platforms in the build directory. This is so we can keep different compiled versions of the different objects and not having to compile every time we use them.

## List of objects

### [dictionary]

An array of dictionary objects (hash tables) that is easily stored and read from a JSON file. Useful for storing and loading a collection of presets.

### [mono_midi]

Handles MIDI note on and off messages in a way that makes sense for monophonic instruments.

### [denormalize]

Converts a number in the [0, 1] range to another number in an arbitrary range and applies a pow curve.

## License

Copyright (C) 2015 Rafael Vega <contacto@rafaelvega.co>
Copyright (C) 2015 Daniel Gómez <danielgomezmarin@gmail.com>

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.  

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
